class Author:
    def __init__(self, name):
        self.name = name
        self.ndocs = 0
        self.production = []
    
    def add(self, doc):
        self.production.append(doc)
        self.ndocs += 1
    
    def getName(self):
        return self.name

    def __str__(self):
        return self.name + " a ecrit " + self.ndoc