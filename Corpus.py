from Author import Author
from Documents.Document import Document
from Documents.Document import DocumentType
import re
import pandas as pd

class Corpus:
    def __init__(self, name):
        self.name = name
        self.collections = {}
        self.authors = {}
        self.id2aut = {}
        self.id2doc = {}
        self.ndoc = 0
        self.naut = 0
        self.vocab = set()
        self.freq = pd.DataFrame(columns=['word', 'freq'])

    def addDoc(self, doc : Document):
        self.id2doc[self.ndoc] = doc.getTitle()
        self.collections[doc.getTitle()] = doc
        self.ndoc += 1

        #Check if we could add new author
        authors = doc.getAuthors()
        for author_name in authors:
            if not author_name in self.authors.keys():
                self.id2aut[self.naut] = author_name
                self.authors[author_name] = Author(author_name)
                self.naut += 1
        
            self.authors[author_name].add(doc)


    def showList(self, type):
        for doc in self.collections.values():
            if doc.getType() == type:
                print(doc)
            

    def search(self, keyword):
        result = []
        for doc in self.collections.values():
            m = re.search("*" + keyword + "*", doc.getText())
            if m:
                result.append(m.group(0))
        return result

    def createVocabulary(self):
        freq_dic = {}

        #Get all vocabulary and freq of words
        for doc in self.collections.values():
            text = self.nettoyer_text(doc.getText())
            words = text.split(' ')
            for word in words:
                self.vocab.add(word)

                if word in freq_dic:
                    freq_dic[word] += 1
                else:
                    freq_dic[word] = 1
            
        #Put dic into the dataframe
        for freq in freq_dic:
            self.freq = self.freq.append({'word': freq,'freq': freq_dic[freq]}, ignore_index=True)

        print(self.freq)

    # Getting list of words
    def getVocab(self):
        return self.vocab

    # Getting the fequency of word pass in argument
    def getFreqOfWord(self, word):
        freq = 0
    
        if word in self.vocab:
            r = self.freq.loc[self.freq['word'] == word]
            freq = r.to_numpy()[0][1]
    
        return freq

    # Getting freq table with pandas structure
    def getFreq(self):
        return self.freq

    def getNWord(self):
        result = 0
        for row in self.freq.itertuples():
            result += row[2]

        return result

    def concorde(self, keyword):
        dataFrame = pd.DataFrame(columns=['contexte gauche', 'motif trouve', 'contexte droit'])
        
        for doc in self.collections.values():
            m = re.search("(.*)" + keyword + "(.*)", doc.getText())
            if(m.group):
                dataFrame.loc[len(dataFrame)] = [m.group(1), keyword, m.group(2)]
        return dataFrame

    def nettoyer_text(self, texte): 
        texte = texte.lower()
        texte = texte.replace('\.|,|;!:', " ")
        texte = texte.replace('(|)|\||[1-9]', "")
        return texte    