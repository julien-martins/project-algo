from prawcore import auth
from Documents.Document import Document
from Documents.Document import DocumentType

class ArxivDocument(Document):
    def __init__(self, title, co_author, url, text, date):
        super().__init__(title, '', url, text, date)
        if isinstance(co_author, list):
            for author in co_author:
                self.authors.append(author['name'])
        else: 
            self.authors.append(co_author['name'])
        self.type = DocumentType.Arvix

    def getCoAuthors(self): 
        return self.co_authors

    def __str__(self):
        return "Je suis un document arxiv"