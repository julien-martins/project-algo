from datetime import datetime as dt

from enum import Enum
class DocumentType(Enum):
    Reddit = 1
    Arvix = 2

class Document():
    def __init__(self, title, author, url, text, date):
        self.title = title
        self.authors = []
        if author != '':
            self.authors.append(author)
        self.date = date
        self.url = url
        self.text = text
        self.type = DocumentType.Reddit

    def getTitle(self):
        return self.title

    def getAuthors(self):
        return self.authors

    def getText(self):
        return self.text

    def toString(self):
        return ""
    def __str__(self):
        return self.title
    def getType(self):
        return self.type