from Documents.Document import Document
from Documents.Document import DocumentType

class RedditDocument(Document):
    def __init__(self, title, author, url, text, date):
        super().__init__(title, author, url, text, date)
        self.type = DocumentType.Reddit
        
    def __str__(self):
        return "Je suis un document reddit"