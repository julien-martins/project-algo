from tkinter import *
import tkinter

from MVC.View import View
from MVC.Controller import Controller
from MVC.Model import Model

class App(tkinter.Tk):
    def __init__(self):
        super().__init__()

        self.geometry("600x300")
        self.title('Projet Algo 2022 by Reynaud Kenan, Martins Julien')

        model = Model()

        view = View(self)

        controller = Controller(model, view)

        view.set_controller(controller)