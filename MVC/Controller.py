from datetime import datetime
import math
import xmltodict
import urllib, urllib.request
import threading

from Documents.RedditDocument import RedditDocument
from Documents.ArxivDocument import ArxivDocument

class Controller():
    def __init__(self, model, view):
        self.model = model
        self.view = view

    def searchTrigger(self):
        self.view.setNotifMessage("En cours de recherche ...")
        self.view.clearRedditFrame()
        self.view.clearArxivFrame()

        self.limit = self.view.getLimitValue()
        self.keyword = self.view.getKeyword()
        
        threading.Thread(target=self.searchDocument).start()

    def analyseTrigger(self):
        self.view.setNotifMessage("En cours d' analyse ...")
        self.model.getCorpusReddit().createVocabulary()
        self.model.getCorpusArxiv().createVocabulary()

        keyword = self.view.getKeyword()

        self.analyseComparative([keyword], self.model.getCorpusReddit(), self.model.getCorpusArxiv())

        #threading.Thread(self.analyseComparative, [keyword], self.model.getCorpusReddit(), self.model.getCorpusArxiv()).start()
        

    # Use arxiv and reddit api to fill the two corpus
    def searchDocument(self):
        #Reddit
        hot_posts = self.model.getReddit().subreddit(self.keyword).hot(limit=self.limit)
        afficher_cles = False
        for i, post in enumerate(hot_posts):
            #if i%10==0: self.view.updateProgress(10)
            if post.selftext == "":
                continue
            date = datetime.fromtimestamp(post.created)
            doc = RedditDocument(post.title, post.author, post.url, post.selftext.replace('\n', " "), date)
            
            #Add to list view
            self.view.addToRedditFrame(post.title)
            
            self.model.addRedditDoc(doc)
        #Arxiv
        url = f'http://export.arxiv.org/api/query?search_query=all:{"+" + self.keyword}&start=0&max_results={self.limit}'
        data = urllib.request.urlopen(url)

        data = xmltodict.parse(data.read().decode('utf-8'))

        for i, entry in enumerate(data["feed"]["entry"]):
            #if i%10==0: print("ArXiv:", i, "/", self.limit)
            doc = ArxivDocument(entry["title"], entry["author"], entry["link"], entry["summary"].replace("\n", ""), entry["published"])

            #Add to list view
            self.view.addToArxivFrame(entry["title"])

            self.model.addArxivDoc(doc)
        self.view.setNotifMessage("Recherche termine !")

    def analyseComparative(self, keywords, corpus1, corpus2):
        result = []
        # TF
        freq1word = 0
        freq2word = 0
        for keyword in keywords:
            freq1word += corpus1.getFreqOfWord(keyword)
            freq2word += corpus2.getFreqOfWord(keyword)

        n1word = corpus1.getNWord()
        n2word = corpus2.getNWord()
        print("Nombre de mot dans le corpus de Reddit: " + str(n1word))
        print("Nombre de mot dans le corpus de ArXiv: " + str(n2word))

        tf1 = freq1word / n1word
        tf2 = freq2word / n2word
        # IDF
        c = 0
        if freq1word != 0 : c += 1
        if freq2word != 0 : c += 1

        if c == 0:
            idf = 0
        else:
            idf = math.log(2/c + 1)
        # TF-IDF
        result.append(freq1word*idf)
        result.append(freq2word*idf)
        
        self.view.setNotifMessage("Analyse termine ...")

        if(result[0] > result[1]):
            self.view.setResultMessage("Le corpus de Reddit est le plus pertinent.")
        elif(result[0] < result[1]):
            self.view.setResultMessage("Le corpus de Arxiv est le plus pertinent.")
        else:
            self.view.setResultMessage("Les deux corpus sont aussi pertinent l'un que l'autre.")
        #return result