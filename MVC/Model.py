from praw.reddit import Reddit

from Corpus import Corpus

class Model():
    def __init__(self):
        self.client_secret = "ymllHE00JKpa-4PutQdZG-IDU7x6yw"
        self.client_id = "sb_i4WgCAX0RTQpBecNy6A"
        self.user_agent = "killercrock64"
        self.reddit = Reddit(client_id=self.client_id, client_secret=self.client_secret, user_agent=self.user_agent)
        self.docs_bruts = []
        self.ndoc = 0
        self.limit = 20
        self.keyword = "math"
        self.corpusReddit = Corpus("Reddit Corpus")
        self.corpusArxiv = Corpus("Arxiv Corpus")
    
    def getReddit(self):
        return self.reddit

    def setLimit(self, limit):
        self.limit = limit

    def getLimit(self):
        return self.limit

    def setKeyword(self, keyword):
        self.keyword = keyword

    def getKeyword(self):
        return self.keyword

    def getCorpusReddit(self):
        return self.corpusReddit
    
    def getCorpusArxiv(self):
        return self.corpusArxiv

    def addRedditDoc(self, doc):
        self.corpusReddit.addDoc(doc)
    
    def addArxivDoc(self, doc):
        self.corpusArxiv.addDoc(doc)