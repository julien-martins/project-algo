from tkinter import *
from tkinter import ttk

from tkscrolledframe import ScrolledFrame

class View():
    def __init__(self, root):
        container = ttk.Frame(root)

        # TITRE
        Label(container, text="Reddit Corpus").grid(column = 1, row=0, pady=10)

        Label(container, text="Arxiv Corpus").grid(column = 0, row=0, pady=10)

        # Reddit Frame
        RedditFrame = ScrolledFrame(container, scrollbars= "vertical", width = 260, height = 100)
        RedditFrame.grid(column = 1, row = 1, padx=10)
        
        RedditFrame.bind_scroll_wheel(root)
        
        self.inner_reddit_frame = RedditFrame.display_widget(Frame)

        # Arxiv Frame
        ArxivFrame = ScrolledFrame(container, scrollbars= "vertical", width = 260, height = 100)
        ArxivFrame.grid(column=0, row=1, padx=10)
        
        ArxivFrame.bind_scroll_wheel(root)
        
        self.inner_arxiv_frame = ArxivFrame.display_widget(Frame)
        # Search Bar
        Label(container, text="MOT-cles").grid(column=0)
        self.value = StringVar()
        keywords = Entry(container, width=30, textvariable=self.value)
        keywords.grid(column=0, padx=10)
        
        self.limitValue = IntVar()
        limit = Scale(container, orient='horizontal', from_=20, to=300, variable=self.limitValue, label="Limit")
        limit.grid()

        searchBtn = Button(container, text="Search", command=self.search_clicked)
        searchBtn.grid()
        analyseBtn = Button(container, text="Analyse", command=self.analyse_clicked)
        analyseBtn.grid()

        self.notification = StringVar()
        Label(container, textvariable=self.notification).grid(column=1, row=2)

        #Analyse result string
        self.result = StringVar()
        Label(container, textvariable=self.result).grid(column=1, row=3)

        container.grid()

        self.controller = None

    def setNotifMessage(self, text):
        self.notification.set(text)

    def setResultMessage(self, text):
        self.result.set(text)

    def stopProgress(self):
        self.progress.stop()

    def search_clicked(self):
        if self.controller:
            self.controller.searchTrigger()

    def analyse_clicked(self):
        if self.controller:
            self.controller.analyseTrigger()

    def getLimitValue(self):
        return self.limitValue.get()

    def getKeyword(self):
        return self.value.get()

    def addToRedditFrame(self, text):
        Label(self.inner_reddit_frame, text=text).pack(anchor='w')
    
    def addToArxivFrame(self, text):
        Label(self.inner_arxiv_frame, text=text).pack(anchor='w')

    def clearFrame(self, frame):
        for widget in frame.winfo_children():
            widget.destroy()
    
    def clearRedditFrame(self):
        self.clearFrame(self.inner_reddit_frame)
    
    def clearArxivFrame(self):
        self.clearFrame(self.inner_arxiv_frame)

    def set_controller(self, controller):
        self.controller = controller